package com.hq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoiTlDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoiTlDemoApplication.class, args);
    }

}
