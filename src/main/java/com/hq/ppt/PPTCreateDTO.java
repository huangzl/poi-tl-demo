package com.hq.ppt;

import lombok.Data;

import java.util.List;
import java.util.Map;


@Data
public class PPTCreateDTO {

    /**
     * ppt页码
     */
    private Integer index;

    /**
     * 动态内容
     */
    private List<Map<String, String>> textList;

    /**
     * 替换内容
     */
    private Map<String, String> replaceMap;

    /**
     * 处理方式 0.替换文本 1.做替换跟以此页作为模板，动态生成ppt内容
     */
    private Integer process = 0;

    /**
     * ppt shape标识
     */
    private String flag;

}
