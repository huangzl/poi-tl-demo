package com.hq.ppt;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class PowerPointConstant {

    /**
     * ppt一行多少个字
     */
    public static final int LINE_MAX_FONT_COUNT = 100;

    /**
     * ppt一行多高
     */
    public static final int LINE_HEIGHT = 22;

    /**
     * 标题高
     */
    public static final int TITLE_HEIGHT = 40;

    /**
     * 图片缩略图的长
     */
    public static final int PIC_WIDTH = 180;

    /**
     * 图片缩略图的宽
     */
    public static final int PIC_HEIGHT = 180;

    /**
     * 内容替换符号
     */
    public static final String CONTENT_SPLIT = "\\$&\\$";

    /**
     * 本地临时文件路径
     */
    public static final String TEMP_PLACE = "tempMarkdown/";
    /**
     * 本地临时ppt文件路径
     */
    public static final String TEMP_PPTX_PLACE = "temporary2ppt/";
    /**
     * 转换后ppt存的地方
     */
    public static final String PPTX_SAVE_PLACE = "result2pptx/";


    /**
     * PPT后缀
     */
    public static final String PPTX_SUFFIX = ".pptx";

    /**
     * ppt模板路徑
     */
    @Getter
    @AllArgsConstructor
    public enum PPTXTemplateEnum {

        IT("template2ppt/ITindex.pptx", "template2ppt/ITtheme.pptx", "template2ppt/ITcenter.pptx", "template2ppt/ITfooter.pptx"),
        FINANCE("template2ppt/financeindex.pptx", "template2ppt/financetheme.pptx", "template2ppt/financecenter.pptx", "template2ppt/financefooter.pptx"),
        OPENCLASS("template2ppt/openclass.pptx", "", "", ""),
        CLASSBEFORE("template2ppt/classbeforeindex.pptx", "template2ppt/classbeforetheme.pptx", "template2ppt/classbeforecenter.pptx", "template2ppt/classbeforefooter.pptx"),
        CLASSMIDDLE("template2ppt/classmiddleindex.pptx", "template2ppt/classmiddletheme.pptx", "template2ppt/classmiddlecenter.pptx", "template2ppt/classmiddlefooter.pptx");

        String index;
        String theme;
        String footer;
        String center;


    }
}