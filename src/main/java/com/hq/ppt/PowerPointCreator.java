package com.hq.ppt;


import com.aspose.slides.*;
import com.deepoove.poi.util.BytePictureUtils;
import com.hq.exception.RRException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ppt生成
 */
@Slf4j
public class PowerPointCreator {

    public static void createPPT(List<PPTCreateDTO> list, String templatePath, String savePath) {
        register();
        Presentation ppt = new Presentation(templatePath);
        ISlideCollection slideList = ppt.getSlides();
        if(slideList.size() == 0) {
            throw new RRException("构造ppt失败");
        }
        Map<Integer, ISlide> map = new HashMap();
        list.forEach(item->{
            map.put(item.getIndex(), slideList.get_Item(item.getIndex()));
        });
        for(int i = 0; i < list.size(); i++ ) {
            PPTCreateDTO item = list.get(i);
            ISlide templateSlide = map.get(item.getIndex());
            IShapeCollection shapes = templateSlide.getShapes();
            int tempIndex = 0;
            for(int j = 0; j < slideList.size(); j++ ) {
                if(templateSlide.equals(slideList.get_Item(j))){
                    tempIndex = j;
                    break;
                }
            }
            //替换文本
            if(item.getProcess() == 0) {
                replaceSlide(ppt, templateSlide, item.getReplaceMap());
            }

            //动态生成固定shape
            if(item.getProcess() == 1) {
                if(item.getReplaceMap() != null) {
                    replaceSlide(ppt, templateSlide, item.getReplaceMap());
                }
                createFixedItem(ppt, slideList, templateSlide, tempIndex, item.getFlag(), shapes, item.getTextList());
            }

        }
        ppt.save(savePath, SaveFormat.Pptx);
    }

    private static void createFixedItem(Presentation ppt, ISlideCollection slideList, ISlide templateSlide, int tempIndex, String itemName, IShapeCollection shapes, List<Map<String, String>> textList){
        //记录目标位置
        List<Integer> indexList = new ArrayList<>();
        for(int i = 0; i < shapes.size(); i++){
            IShape shape = shapes.get_Item(i);
            if(itemName.equals(shape.getAlternativeTextTitle())) {
                indexList.add(i);
            }
        }
        int signalCount = indexList.size();
        int needCount = textList.size();
        if(signalCount > 0) {
            //一页item数量多于需求量不需要分页 否则分页处理
            if(signalCount > needCount) {
                diuShape(ppt, indexList, textList, shapes);
            } else {
                int currPage = 0;
                int totalPage = (needCount % signalCount) == 0 ? needCount / signalCount : (needCount / signalCount) + 1;
                while (currPage < totalPage) {
                    diuShape(ppt, indexList, textList.subList(currPage*signalCount, textList.size()), slideList.insertClone(tempIndex + currPage, templateSlide).getShapes());
                    currPage++;
                }
                slideList.remove(templateSlide);
            }
        }
    }

    /**
     * 处理动态固定元素
     * @param ppt
     * @param indexList
     * @param textList
     * @param shapes
     */
    private static void diuShape(Presentation ppt, List<Integer> indexList, List<Map<String, String>> textList, IShapeCollection shapes){
        List<IShape> deleteShapes = new ArrayList<>();
        for(int i = 0; i < indexList.size(); i++){
            IShape iShape = shapes.get_Item(indexList.get(i));
            //如果用不完就把多余的删除了
            if(i >= textList.size()) {
                deleteShapes.add(iShape);
            } else {
                if(iShape instanceof IGroupShape) {
                    IShapeCollection childs = ((IGroupShape)iShape).getShapes();
                    replaceShape(ppt, childs, textList.get(i));
                } else {
                    replaceShape(ppt, iShape, textList.get(i));
                }
            }
        }
        deleteShapes.forEach(item->{
            shapes.remove(item);
        });

    }
    /**
     * 替换slide内容
     * @param slideList
     * @param replaceMap
     */
    private static void replaceSlide(Presentation ppt, ISlideCollection slideList, Map<String, String> replaceMap) {
        //遍历每一页PPT构造
        for(int j = 0; j<slideList.size(); j++) {
            ISlide slide = slideList.get_Item(j);
            replaceSlide(ppt, slide, replaceMap);
        }
    }

    /**
     * 单页替换slide内容
     * @param slide
     * @param replaceMap
     */
    private static void replaceSlide(Presentation ppt, ISlide slide, Map<String, String> replaceMap) {
        //先替换文本
        if (replaceMap != null) {
            IShapeCollection shapeList = slide.getShapes();
            replaceShape(ppt, shapeList, replaceMap);
        }

    }

    /**
     * 替换shape内容
     * @param shapeList
     * @param replaceMap
     */
    private static void replaceShape(Presentation ppt, IShapeCollection shapeList, Map<String, String> replaceMap) {
        for (int o = 0; o < shapeList.size(); o++) {
            IShape shape = shapeList.get_Item(o);
            //判断文本框
            if (shape instanceof IAutoShape) {
                replaceText((IAutoShape) shape, replaceMap);
            }
//
//            if (shape instanceof IVideoFrame) {
//                try {
//                    replaceVideo(ppt, (IVideoFrame) shape, replaceMap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
        }
    }

    /**
     * 替换shape内容
     * @param shape
     * @param replaceMap
     */
    private static void replaceShape(Presentation ppt, IShape shape, Map<String, String> replaceMap) {
        if (shape instanceof IAutoShape) {
            replaceText((IAutoShape) shape, replaceMap);
        }
        if (shape instanceof IVideoFrame) {
            try {
                replaceVideo(ppt, (IVideoFrame) shape, replaceMap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 替换视频
     * @param iVideoFrame
     * @param textMap
     */
    public static void replaceVideo(Presentation ppt, IVideoFrame iVideoFrame, Map<String, String> textMap) throws IOException {
        String url = textMap.get("url");
        InputStream fileStream = BytePictureUtils.getUrlPictureStream(url);
        IVideo video = ppt.getVideos().addVideo(fileStream, LoadingStreamBehavior.KeepLocked);
        iVideoFrame.setEmbeddedVideo(video);
        iVideoFrame.setPlayMode(VideoPlayModePreset.Auto);
        iVideoFrame.setVolume(AudioVolumeMode.Loud);
        String thumbnailUri = "http://hq-expert.oss-cn-shenzhen.aliyuncs.com/hangjia/pc/v2/image/play-dh5dpe03na5qemq.jpg";
        iVideoFrame.getPictureFormat().getPicture().setImage(ppt.getImages().addImage(BytePictureUtils.getUrlPictureStream(thumbnailUri)));
    }

    /**
     * 替换文本
     * @param iAutoShape
     * @param textMap
     */
    private static void replaceText(IAutoShape iAutoShape, Map<String, String> textMap) {
        IParagraphCollection textParagraphList = iAutoShape.getTextFrame().getParagraphs();
        for (IParagraph textParagraph : textParagraphList) {
            //正则表达式匹配${}标识符的text
            String text = textParagraph.getText();
            if (StringUtils.isEmpty(text)) {
                continue;
            }

            String regex = "\\$\\{.*?\\}";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(text);
            List<String> keys = new ArrayList<>();
            while (matcher.find()) {
                keys.add(matcher.group());
            }
            for (String key : keys) {
                String textKey = key.substring(2, key.length() - 1);
                if (StringUtils.isEmpty(textKey)) {
                    continue;
                }
                if(textMap.get(textKey) != null) {
                    text = text.replace(key, textMap.get(textKey));
                }
            }
            textParagraph.setText(text);
        }
    }


    /**
     * aspose破解
     */
    private static void register() {
        try {
            License license = new License();
            license.setLicense(System.getProperty("user.dir") + "/license.xml");
            if(!license.isLicensed()){
                throw new RRException("注册aspose失败，生成失败");
            };
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new RRException("注册aspose失败，生成失败");
        }
    }
}
