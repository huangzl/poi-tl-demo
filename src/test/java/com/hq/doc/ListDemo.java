package com.hq.doc;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.NumbericRenderData;
import com.deepoove.poi.data.TextRenderData;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * http://deepoove.com/poi-tl/#_2min入门
 * 列表模板{{*var}}
 **/
public class ListDemo {

    @Test
    public void test1() throws IOException {

        NumbericRenderData numbericRenderData = new NumbericRenderData(new ArrayList<TextRenderData>() {
            {
                add(new TextRenderData("Plug-in grammar"));
                add(new TextRenderData("Supports word text, header..."));
                add(new TextRenderData("Not just templates, but also style templates"));
            }
        });
        numbericRenderData.setNumFmt(NumbericRenderData.FMT_DECIMAL);
        XWPFTemplate template = XWPFTemplate.compile("template/template5.docx").render(new HashMap<String, Object>() {{
            put("feature", numbericRenderData);
            put("title", "列表模板{{*var}}");
        }});
        FileOutputStream out = new FileOutputStream("template/result/out_template5.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }
}