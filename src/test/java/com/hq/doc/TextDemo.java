package com.hq.doc;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.HyperLinkTextRenderData;
import com.deepoove.poi.data.TextRenderData;
import com.deepoove.poi.data.style.Style;
import org.junit.jupiter.api.Test;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHighlightColor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;


/**
 * http://deepoove.com/poi-tl/#_2min入门
 * 文本模板{{var}}
 **/
public class TextDemo {

    @Test
    public void test1() throws IOException {

        XWPFTemplate template = XWPFTemplate.compile("template/template1.docx").render(new HashMap<String, Object>(){{
            put("title", "Poi-tl 模板引擎");
        }});
        FileOutputStream out = new FileOutputStream("template/result/out_template1.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }

    @Test
    public void test2() throws IOException {


        XWPFTemplate template = XWPFTemplate.compile("template/template2.docx").render( new HashMap<String, Object>(){{
            put("author", new TextRenderData("CC00FF", "Sayi卅一"));
            Style style = new Style();
            style.setHighlightColor(STHighlightColor.BLUE);
            style.setBold(true);
            style.setItalic(true);
            put("test", new TextRenderData( "测试嘻嘻嘻嘻嘻嘻嘻",style));
            put("introduce", "http://www.deepoove.com");
            put("link", new HyperLinkTextRenderData("website.", "http://www.deepoove.com"));
        }});
        FileOutputStream out = new FileOutputStream("template/result/out_template2.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }
}