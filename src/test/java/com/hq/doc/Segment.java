package com.hq.doc;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.DocxRenderData;
import com.hq.doc.model.SegmentData;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * http://deepoove.com/poi-tl/#_2min入门
 * 图片模板{{@var}}
 **/
public class Segment {

    @Test
    public void test1() throws IOException {


        List<SegmentData> segments = new ArrayList<SegmentData>();
        SegmentData s1 = new SegmentData();
        s1.setTitle("经常抱怨的自己");
        s1.setContent("每个人生活得都不容易，经常向别人抱怨的人，说白了就是把对方当做“垃圾场”，你一股脑地将自己的埋怨与不满倒给别人，自己倒是爽了，你有考虑过对方的感受吗？对方的脸上可能一笑了之，但是心里可能有一万只草泥马奔腾而过。");
        segments.add(s1);

        SegmentData s2 = new SegmentData();
        s2.setTitle("拖拖拉拉的自己");
        s2.setContent("能够今天做完的事情，不要拖到明天，你的事情没有任何人有义务去帮你做；不要做“宅男”、不要当“宅女”，放假的日子约上三五好友出去转转；经常动手做家务，既能分担伴侣的负担，又有一个干净舒适的环境何乐而不为呢？");
        segments.add(s2);

        XWPFTemplate template = XWPFTemplate.compile("template/template6.docx").render(new HashMap<String, Object>() {{
            put("docx_word", new DocxRenderData(new File("template/segment.docx"), segments));
            put("title", "文档模板{{+var}}");
        }});
        FileOutputStream out = new FileOutputStream("template/result/out_template6.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }
}