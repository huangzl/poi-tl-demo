package com.hq.doc;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.util.BytePictureUtils;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

/**
 * http://deepoove.com/poi-tl/#_2min入门
 * 图片模板{{@var}}
 **/
public class PicDemo {

    @Test
    public void test1() throws IOException {

        XWPFTemplate template = XWPFTemplate.compile("template/template3.docx").render(new HashMap<String, Object>() {{
            // 本地图片
            put("localPicture", new PictureRenderData(120, 120, "template/pic/demo.jpeg"));
            // 图片流文件
            put("localBytePicture", new PictureRenderData(100, 120, ".png", new FileInputStream("template/pic/fried_rice.png")));
            // 网络图片
            put("urlPicture", new PictureRenderData(100, 100, ".jpg", BytePictureUtils.getUrlBufferedImage("http://img3.redocn.com/20110109/Redocn_2010121309121846.jpg")));
            // java 图片
//            put("bufferImagePicture", new PictureRenderData(100, 120, ".png", bufferImage));
        }});
        FileOutputStream out = new FileOutputStream("template/result/out_template3.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }
}