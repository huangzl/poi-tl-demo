package com.hq.doc;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.data.MiniTableRenderData;
import com.deepoove.poi.data.RowRenderData;
import com.deepoove.poi.data.TextRenderData;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;

/**
 * http://deepoove.com/poi-tl/#_2min入门
 * 图片模板{{#var}}
 **/
public class TableDemo {

    @Test
    public void test1() throws IOException {

        RowRenderData header = RowRenderData.build(new TextRenderData("CC00FF", "姓名"), new TextRenderData("CC00FF", "学历"));

        RowRenderData row0 = RowRenderData.build("张三", "研究生");
        RowRenderData row1 = RowRenderData.build("李四", "博士");
        RowRenderData row2 = RowRenderData.build("王五", "博士后");


        XWPFTemplate template = XWPFTemplate.compile("template/template4.docx").render(new HashMap<String, Object>() {{
            put("table", new MiniTableRenderData(header, Arrays.asList(row0, row1, row2)));
            put("title", "表格模板{{#var}}");
        }});
        FileOutputStream out = new FileOutputStream("template/result/out_template4.docx");
        template.write(out);
        out.flush();
        out.close();
        template.close();
    }
}