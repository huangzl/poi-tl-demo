package com.hq.ppt;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public  class TextTest {

    @Test
    void createText() {

        List<PPTCreateDTO> list = new ArrayList<>();
        //首页
        PPTCreateDTO index = new PPTCreateDTO();
        index.setIndex(0);
        index.setProcess(0);
        Map<String, String> textMap = new HashMap<>(2);
        textMap.put("lname", "开学典礼");
        textMap.put("kname", "初级会计通过课程");
        index.setReplaceMap(textMap);
        list.add(index);

        //要求页
        PPTCreateDTO requirement = new PPTCreateDTO();
        requirement.setIndex(2);
        requirement.setProcess(0);
        textMap = new HashMap<>(1);
        textMap.put("text", "关于JAVA集合使用双大括号初始化");
        requirement.setReplaceMap(textMap);
        list.add(requirement);



        PowerPointCreator.createPPT(list, "template2ppt/ITindex.pptx", "template2ppt/result/ITindex.pptx");
    }

    /**
     * 创建动态模板
     */
    @Test
    public void createDyText(){

        List<PPTCreateDTO> list = new ArrayList<>();

        //议题页
        List<String> topicList = new ArrayList(){{
            add("议题1");
            add("议题2");
            add("议题3");
            add("议题4");
            add("议题5");
            add("议题6");
            add("议题7");
            add("议题8");
            add("议题9");
        }};
        List<Map<String, String>> topicTextList = new ArrayList<>();
        PPTCreateDTO topic = new PPTCreateDTO();
        topic.setIndex(0);
        topic.setProcess(1);
        topic.setFlag("fixedItem");
        for (int i = 0; i < topicList.size(); i++) {
            Map<String, String> topicMap = new HashMap<>(2);
            topicMap.put("i", (i + 1) + "");
            topicMap.put("title", topicList.get(i));
            topicTextList.add(topicMap);
        }
        topic.setTextList(topicTextList);
        list.add(topic);

        PowerPointCreator.createPPT(list, "template2ppt/动态文本.pptx", "template2ppt/result/动态文本.pptx");
    }


    /**
     * 创建普通动态模板
     */
    @Test
    public void createDyText2(){

        List<PPTCreateDTO> list = new ArrayList<>();


        //首页
        PPTCreateDTO index = new PPTCreateDTO();
        index.setIndex(0);
        index.setProcess(0);
        Map<String, String> textMap = new HashMap<>(2);
        textMap.put("lname", "开学典礼");
        textMap.put("kname", "初级会计通过课程");
        index.setReplaceMap(textMap);
        list.add(index);
        //要求页
        PPTCreateDTO requirement = new PPTCreateDTO();
        requirement.setIndex(1);
        requirement.setProcess(0);
        textMap = new HashMap<>(1);
        textMap.put("text", "关于JAVA集合使用双大括号初始化");
        requirement.setReplaceMap(textMap);
        list.add(requirement);

        //议题页
        List<String> topicList = new ArrayList(){{
            add("议题1");
            add("议题2");
            add("议题3");
            add("议题4");
            add("议题5");
            add("议题6");
            add("议题7");
            add("议题8");
            add("议题9");
        }};
        List<Map<String, String>> topicTextList = new ArrayList<>();
        PPTCreateDTO topic = new PPTCreateDTO();
        topic.setIndex(2);
        topic.setProcess(1);
        topic.setFlag("fixedItem");
        for (int i = 0; i < topicList.size(); i++) {
            Map<String, String> topicMap = new HashMap<>(2);
            topicMap.put("i", (i + 1) + "");
            topicMap.put("title", topicList.get(i));
            topicTextList.add(topicMap);
        }
        topic.setTextList(topicTextList);
        list.add(topic);

        PowerPointCreator.createPPT(list, "template2ppt/普通-动态文本.pptx", "template2ppt/result/普通-动态文本.pptx");
    }

}