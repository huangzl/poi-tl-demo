package com.hq.ppt;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class VideoTest {

    @Test
    void createVideo() {

        List<PPTCreateDTO> list = new ArrayList<>();

        List<Map<String, String>> videoTextList = new ArrayList<>();
        Map<String, String> tempMap = new HashMap<>(1);
        tempMap.put("url", "http://localhost/ppt/f659524c0f4d46aea9522d6464c0f21c.mp4");
        videoTextList.add(tempMap);

        PPTCreateDTO video = new PPTCreateDTO();
        video.setIndex(0);
        video.setProcess(1);
        video.setTextList(videoTextList);
        video.setFlag("fixedVideo");

        Map<String, String> textMap = new HashMap<>(1);
        textMap.put("title", "视频演示");
        video.setReplaceMap(textMap);

        list.add(video);
        PowerPointCreator.createPPT(list, "template2ppt/视频.pptx", "template2ppt/result/视频.pptx");
    }

    @Test
    void createVideo2() {

        List<PPTCreateDTO> list = new ArrayList<>();

        List<Map<String, String>> videoList = new ArrayList<>();
        Map<String, String> tempMap = new ConcurrentHashMap<>(1);
        tempMap.put("url", "http://localhost/ppt/f659524c0f4d46aea9522d6464c0f21c.mp4");
        videoList.add(tempMap);
        tempMap = new ConcurrentHashMap<>(1);
        tempMap.put("url", "http://localhost/ppt/f659524c0f4d46aea9522d6464c0f21c.mp4");
        videoList.add(tempMap);

        PPTCreateDTO video = new PPTCreateDTO();
        video.setIndex(0);
        video.setProcess(1);
        video.setTextList(videoList);
        video.setFlag("fixedVideo");

        Map<String, String> textMap = new HashMap<>(1);
        textMap.put("title", "视频演示");
        video.setReplaceMap(textMap);

        list.add(video);
        PowerPointCreator.createPPT(list, "template2ppt/多视频.pptx", "template2ppt/result/多视频.pptx");
    }

}